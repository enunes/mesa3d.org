---
title:    "Mesa 10.6.3 is released"
date:     2015-07-26 00:00:00
category: releases
tags:     []
---
[Mesa 10.6.3](https://docs.mesa3d.org/relnotes/10.6.3.html) is released. This is a bug-fix
release.
