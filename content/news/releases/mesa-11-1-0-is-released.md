---
title:    "Mesa 11.1.0 is released"
date:     2015-12-15 00:00:00
category: releases
tags:     []
---
[Mesa 11.1.0](https://docs.mesa3d.org/relnotes/11.1.0.html) is released. This is a new
development release. See the release notes for more information about
the release.
