---
title:    "Mesa 20.0.2 is released"
date:     2020-03-18 00:00:00
category: releases
tags:     []
---
[Mesa 20.0.2](https://docs.mesa3d.org/relnotes/20.0.2.html) is released. This is a bug fix
release.
