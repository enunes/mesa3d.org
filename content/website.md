---
Title: About Mesa3D.org
aliases:
  - /webmaster.html
---
This website consist of three parts:

1. [www.mesa3d.org](https://www.mesa3d.org/): This is the landing-site for
   the Mesa3D project. It contains some introductory information about the
   project, but consists mostly of links elsewhere.

   It's maintained as a [Hugo](https://gohugo.io/) website, and its
   source-code can be found [here]({{< param GitLabURL >}}).

2. [docs.mesa3d.org]({{< param DocsURL >}}): This is the technical
   documentation for the current version of Mesa3D. It consists of a lot more
   in-depth information about how Mesa3D works, and how to use it.

   It's maintained as a set of [Sphinx](https://www.sphinx-doc.org/)
   documentation, and it's source code can be found in the
   [`doc/` folder](https://gitlab.freedesktop.org/mesa/mesa/-/tree/master/docs)
   of the [main mesa repository](https://gitlab.freedesktop.org/mesa/mesa/).

3. [archive.mesa3d.org](https://archive.mesa3d.org/): This is the archive of
   releases of Mesa3D. This hosts the tarballs of all Mesa3D releases.

If you want to do changes to either of the first two sites, please submit a
merge-request to the respective repository.
