# Mesa3D.org

The front-page and news-archive of Mesa3D.org, the open source
implementations of OpenGL, OpenGL ES, Vulkan, OpenCL, and more.

You can read more about the website itself [here](./content/website.md).

## Contributing

This website is built using [Hugo](https://gohugo.io/), and can be built
locally by following the instructions below.

The site is hosted by Freedesktop.org, on their [GitLab
Pages](https://gitlab.freedesktop.org/help/user/project/pages/index.md)
infrastructure.

### Building locally

```sh
$ git clone https://gitlab.freedesktop.org/mesa/mesa3d.org.git
```

Then to view the site in your browser, run Hugo and open up the link:
```sh
$ hugo server
...
...
Web Server is available at http://localhost:1313/ (bind address 127.0.0.1)
Press Ctrl+C to stop
```

You can read more about how to use Hugo, by reading [the Hugo
documentation](https://gohugo.io/documentation/).

### Requesting changes

Changes can be sent as normal [GitLab Merge
Requests](https://gitlab.freedesktop.org/help#merge-requests).
