---
title:    "Mesa 20.2.5 is released"
date:     2020-12-16 14:00:33
category: releases
tags:     []
---
[Mesa 20.2.5](https://docs.mesa3d.org/relnotes/20.2.5.html) is released. This is a bug fix release.

This release has been supersceeded by 20.2.6, which contians a critical bug fix for ACO. Please do not use this release.
