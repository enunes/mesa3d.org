---
title:    "Mesa 12.0.1 is released"
date:     2016-07-08 00:00:00
category: releases
tags:     []
summary:  "[Mesa 12.0.1](https://docs.mesa3d.org/relnotes/12.0.1.html) is released. This is a bug-fix
release, resolving build issues in the r600 and radeonsi drivers."
---
[Mesa 12.0.1](https://docs.mesa3d.org/relnotes/12.0.1.html) is released. This is a bug-fix
release, resolving build issues in the r600 and radeonsi drivers.

[Mesa 12.0.0](https://docs.mesa3d.org/relnotes/12.0.0.html) is released. This is a new
development release. See the release notes for more information about
the release.
