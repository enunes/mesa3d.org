---
title:    "Mesa 20.1.3 is released"
date:     2020-07-08 22:58:07
category: releases
tags:     []
---
[Mesa 20.1.3](https://docs.mesa3d.org/relnotes/20.1.3.html) is released. This is a bug fix release.
