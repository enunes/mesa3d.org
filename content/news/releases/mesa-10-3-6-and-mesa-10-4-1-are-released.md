---
title:    "Mesa 10.3.6 and Mesa 10.4.1 are released"
date:     2014-12-29 00:00:00
category: releases
tags:     []
---
[Mesa 10.3.6](https://docs.mesa3d.org/relnotes/10.3.6.html) and [Mesa
10.4.1](https://docs.mesa3d.org/relnotes/10.4.1.html) are released. These are bug-fix releases
from the 10.3 and 10.4 branches, respectively.
