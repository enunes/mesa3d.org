---
title:    "March 23, 2000"
date:     2000-03-23 00:00:00
category: misc
tags:     []
summary:  "I've just upload the Mesa 3.2 beta 1 files to SourceForge at
[https://sourceforge.net/project/filelist.php?group\_id=3](https://sourceforge.net/project/showfiles.php?group_id=3)"
---
I've just upload the Mesa 3.2 beta 1 files to SourceForge at
[https://sourceforge.net/project/filelist.php?group\_id=3](https://sourceforge.net/project/showfiles.php?group_id=3)

3.2 (note even number) is a stabilization release of Mesa 3.1 meaning
it's mainly just bug fixes.

Here's what's changed:

``` 
    Bug fixes:
        - mixed drawing of lines and bitmaps sometimes had wrong colors
        - added missing glHintPGI() function
        - fixed a polygon culling bug
        - fixed bugs in gluPartialDisk()
        - Z values in selection mode were wrong
        - added missing tokens:
            GL_SMOOTH_POINT_SIZE_RANGE
            GL_SMOOTH_POINT_SIZE_GRANULARITY
            GL_SMOOTH_LINE_WIDTH_RANGE
            GL_SMOOTH_LINE_WIDTH_GRANULARITY
            GL_ALIASED_POINT_SIZE_RANGE
            GL_ALIASED_LINE_WIDTH_RANGE
        - fixed glCopyPixels when copying from back to front buffer
        - GL_EXT_compiled_vertex_array tokens had _SGI suffix instead of _EXT
        - glDrawRangeElements(GL_LINES, 0, 1, 2, type, indices) was broken
        - glDeleteTextures() didn't decrement reference count correctly
        - GL_SRCA_ALPHA_SATURATE blend mode didn't work correctly
        - Actual depth of transformation matrix stacks was off by one
        - 24bpp visuals didn't address pixels correctly
        - mipmap level of detail (lambda) calculation simplified, more accurate
        - 101691 - Polygon clipping and GL_LINE
        - 101928 - Polygon clipping and GL_LINE (same fix as above)
        - 101808 - Non-glVertexArrays tristrip bug
        - 101971 - find_last_3f on Dec OSF (worked around)
        - 102369 - segv on dec osf (possibly a duplicate of the above)
        - 102893 - orientations of modelview cause segfault
    New:
        - updated SVGA Linux driver
        - added the MESA_FX_NO_SIGNALS env var, see docs/README.3DFX
        - build libGLw.a (Xt/OpenGL drawing area widget) library by default
        - changed -O2 to -O3 for a number of gcc configs
    Changes:
        - glXCopyContext's mask parameter is now unsigned long, per GLX spec
```

Please report any problems with this release ASAP. Bugs should be filed
on the Mesa3D website at sourceforge.

After 3.2 is wrapped up I hope to release 3.3 beta 1 soon afterward.

\-- Brian
