---
title:    "Mesa 17.2.8 is released"
date:     2017-12-22 00:00:00
category: releases
tags:     []
summary:  "[Mesa 17.2.8](https://docs.mesa3d.org/relnotes/17.2.8.html) is released. This is a bug-fix
release."
---
[Mesa 17.2.8](https://docs.mesa3d.org/relnotes/17.2.8.html) is released. This is a bug-fix
release.

{{< alert type="info" title="Note" >}}
It is anticipated that 17.2.8 will be the final release in the
17.2 series. Users of 17.2 are encouraged to migrate to the 17.3 series
in order to obtain future fixes.
{{< /alert >}}
