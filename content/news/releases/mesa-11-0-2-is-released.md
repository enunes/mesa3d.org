---
title:    "Mesa 11.0.2 is released"
date:     2015-09-28 00:00:00
category: releases
tags:     []
---
[Mesa 11.0.2](https://docs.mesa3d.org/relnotes/11.0.2.html) is released. This is a bug-fix
release.
