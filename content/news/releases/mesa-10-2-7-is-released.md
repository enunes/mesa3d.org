---
title:    "Mesa 10.2.7 is released"
date:     2014-09-06 00:00:00
category: releases
tags:     []
---
[Mesa 10.2.7](https://docs.mesa3d.org/relnotes/10.2.7.html) is released. This is a bug-fix
release.
