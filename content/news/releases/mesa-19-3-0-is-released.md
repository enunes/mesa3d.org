---
title:    "Mesa 19.3.0 is released"
date:     2019-12-12 00:00:00
category: releases
tags:     []
---
[Mesa 19.3.0](https://docs.mesa3d.org/relnotes/19.3.0.html) is released. This is a new
development release. See the release notes for mor information about
this release.
