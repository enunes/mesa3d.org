---
title:    "Mesa 20.2.3 is released"
date:     2020-11-23 11:32:14
category: releases
tags:     []
---
[Mesa 20.2.3](https://docs.mesa3d.org/relnotes/20.2.3.html) is released. This is a bug fix release.
