---
title:    "Mesa 18.1.9 is released"
date:     2018-09-24 00:00:00
category: releases
tags:     []
summary:  "[Mesa 18.1.9](https://docs.mesa3d.org/relnotes/18.1.9.html) is released. This is a bug-fix
release."
---
[Mesa 18.1.9](https://docs.mesa3d.org/relnotes/18.1.9.html) is released. This is a bug-fix
release.

{{< alert type="info" title="Note" >}}
It is anticipated that 18.1.9 will be the final release in the
18.1 series. Users of 18.1 are encouraged to migrate to the 18.2 series
in order to obtain future fixes.
{{< /alert >}}
